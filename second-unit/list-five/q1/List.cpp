#include "List.hpp"
#include <cstddef>
#include <iostream>

using namespace std;

List::List()
{
    head = NULL;
    tail = NULL;
}

List::~List() {
    Node *current;
    while (head != NULL) {
        current = head;
        head = head->next;
        delete current;
    }
}

int List::listSize() {
    if (head == NULL)
        return 0;

    Node *h = head;
    int size = 0;
    
    while (h) {
        h = h->next;
        size++;
    }
    return size;
}

void List::print() {
    Node *h = head;
    if (head == NULL)
        cout << "Lista vázia!" << endl;
    else {
        while (h != NULL) {
            if(h->next == NULL) {
               cout << h->value << " ";
            } else 
                cout << h->value << "-->";
            h = h->next;
        }
    }
    cout << endl;
}


void List::addElementIndex(int value, int index) {
    if (index < 0 || index > listSize() + 1) {
        cout << "Posição inválida!" << endl;
        return;
    }

    Node *aux = new Node(value);
    aux->value = value;
    aux->next = NULL;

    if (index == 0) {
        aux->next = head;
        head = aux;
    } else {
        Node *h = head;

        while (--index > 0)
            h = h->next;
    
        aux->next = h->next;
        h->next = aux;
    }
}


void List::addElement(int value) {
    Node *aux = new Node(value);

    if (head == NULL) {
        head = aux;
        tail = aux;
    } else {
        aux->next = head;
        head = aux;
    }
    cout << "Adicionando: " << value << endl;
}

void List::addLast(int value) {
    Node *aux = new Node(value);

    if (head == NULL) {
        head = aux;
        tail = aux;
    } else {
        tail->next = aux;
        tail = aux;
    }
    cout << "Adicionando no fim: " << value << endl;
}

void List::removeElementIndex(int index) {
    Node *aux = head;

    if (index == 0) {
        head = aux->next;
        delete aux;
        return;
    }
    for (int i = 0; i < index - 1; i++)
        aux = aux->next;

    Node *aux2 = aux->next;
    aux->next = aux2->next;
    delete aux2;
}

void List::removeFirstElement() {
    Node *firstNode;
    if (head != NULL) {
        firstNode = head;
        head = head->next;
        delete firstNode;
    }
}

void List::removeLastElement() {
    Node *aux = head;
    while (aux->next->next != NULL)
        aux = aux->next;

    Node *lastNode = aux->next;
    aux->next = NULL;
    delete lastNode;
}

int List::findIndex(int value) {
    Node *aux = head;
    int index = 0;

    while (aux != NULL) {
        if (aux->value == value)
            return index;
        aux = aux->next;
        index++;
    }
    return -1;
}