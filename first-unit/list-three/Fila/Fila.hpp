#ifndef FILA_HPP
#define FILA_HPP

class Fila
{
private:
    int *vetor;
    int capacidade;
    int contador;
    int primeiro;
    int ultimo;

public:
    Fila(int);
    ~Fila();
    void enfileirar(int);
    int desenfileirar();
    int tamanhoFila();
    bool filaCheia();
    bool filaVazia();
    int elementoTopo();
    int elementoFundo();
    void mostrarFila();
};

#endif