#include <iostream>

using namespace std;

void insertionSort(int , int );

int main() {
  int n;
  
  cout << "Quantos elementos deseja adicionar: ";
  cin >> n;

  int s[n];

  for(int i = 0; i < n ; i++) 
    cin >> s[i];
    
  insertionSort(s, n);

  for(int i = 0; i < n ; i++) {
    cout << s[i] << " ";
  }

  cout << endl;
}


void insertionSort(int vetor[], int n) {
  int chave, j;
  
  for(int i = 1; i < n; i++) {
    chave = vetor[i];
    j = i - 1; // i inicializa com 1, para que nessa linha, quando acontecer a subtração, não fique negativo.
    
    while( j >= 0 && vetor[j] > chave) {
      vetor[j + 1] = vetor[j];
      j -= 1;
    }

    vetor[j+1] = chave;
  }
}
