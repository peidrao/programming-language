#ifndef CIRCULARLIST_HPP
#define CIRCULARLIST_HPP

#include "Node.hpp"
#include <iostream>
#include <cstddef>

using namespace std;
class CircularList
{
private:
    Node *head;

public:
    CircularList();
    ~CircularList();
    int listSize();
    void printList();
    void addNode(int);
    int findNode(int);
};

#endif
