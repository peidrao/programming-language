#ifndef SET_HPP
#define SET_HPP

#include <iostream>
using namespace std;

const int CAPACITY = 11;
const int STEPSIZE = 3;

class Set {
private:
    int* table;
    int elements;
    int capacity;

public:
    Set();
    Set(int);

    Set* Union(Set*, Set*);
    Set* Inter(Set*, Set*);
    ~Set();
    void insert(int);
    int hashValue(int key, int j);

    bool get(int);
    int getCapacity();
    void print();
};

#endif