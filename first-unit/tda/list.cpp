#include <cstdlib>
#include "list.hpp"

using namespace std;

list *criar(int capacidade)
{
    list *newList = (list *)malloc(sizeof(list));
    if (newList != NULL)
    {
        newList->capacidade = capacidade;
        newList->tamanho = 0;
        newList->elementos = (int *)malloc(capacidade * sizeof(int));

        if (newList->elementos == NULL)
        {
            free(newList);
            newList = NULL;
        }
    }
    return newList;
}

void destruir(list *l)
{
    free(l->elementos);
    free(l);
}

/* Assumindo uma posção válida.  [0, tamanho ] */
int adicionar(list *l, int valor, int posicao)
{
    if (l->tamanho < l->capacidade)
    {
        if (posicao == l->tamanho)
            l->elementos[posicao] = valor;
        else
        {
            for (int i = l->tamanho; i > posicao; i--)
                l->elementos[i] = l->elementos[i - 1];
            l->elementos[posicao] = valor;
        }
        l->tamanho += 1;
        return 1;
    }
    else
        return 0;
}