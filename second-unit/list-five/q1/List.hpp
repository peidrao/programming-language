#ifndef LIST_HPP
#define LIST_HPP

#include "Node.hpp"
class List
{
private:
    Node *head;
    Node *tail;

public:
    List();
    ~List();
    void print();
    int listSize();
    void addElementIndex(int, int); /* addElementIndex: Adiciona elemento em uma posição específica */
    void addElement(int);           /* addElement: Adiciona um valor na primeira posição da lista */
    void addLast(int);              /* addLast: Adicionando elemento no final da lista */
    void removeElementIndex(int);   /* removeElementIndex: Remove um elemento dado uma posição que se deseja eliminar. */
    void removeFirstElement();      /* removeFirstElement: Remove primeiro elemento da lista  */
    void removeLastElement();       /* removeLastElement: Remove último elemento da lista */
    int findIndex(int);             /* findIndex: Procura um elemento dentro da lista e retorna a posição dele, caso encontrado */
};

#endif