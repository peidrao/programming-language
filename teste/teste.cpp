#include <iostream>

using namespace std;

void mergeSort(int*, int, int);
void  merge(int*, int, int, int);


int main() {
  int n;
  cin >> n;
  int s[n];

  for(int i = 0 ; i < n ; i++) {
    cin >> s[i];
  }

  mergeSort(s, 0, n-1);
    

    for(int i = 0 ; i < n ; i++) {
    cout <<  s[i];
  }

  return 0;
}

void merge(int vetor[], int inicio, int meio, int fim) {
  int com1 = inicio;
  int com2 = meio+1;
  int comAux = 0;
  int tam = fim - inicio +1;

  int vetorAux[tam];

       while(com1 <= meio && com2 <= fim){

        if(vetor[com1] < vetor[com2]) {

            vetorAux[comAux] = vetor[com1];

            com1++;

        } else {

            vetorAux[comAux] = vetor[com2];

            com2++;

        }

        comAux++;

    }


    while(com1 <= meio){  //Caso ainda haja elementos na primeira metade

        vetorAux[comAux] = vetor[com1];

        comAux++;

        com1++;

    }


    while(com2 <= fim) {   //Caso ainda haja elementos na segunda metade

        vetorAux[comAux] = vetor[com2];

        comAux++;

        com2++;

    }


    for(comAux = inicio; comAux <= fim; comAux++){    //Move os elementos de volta para o vetor original

        vetor[comAux] = vetorAux[comAux-inicio];

    }



}


void mergeSort(int vetor[], int inicio, int fim) {
  if(inicio < fim ){
    int meio = (inicio + fim) / 2;


    mergeSort(vetor,inicio, meio );
    mergeSort(vetor, meio + 1, fim);
    merge(vetor, inicio, meio, fim);
  }
}



