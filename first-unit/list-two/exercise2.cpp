#include <iostream>

using namespace std;

void bubbleSort(int*, int );

int main() {
  int n;
  
  cout << "Digite o tamanho do vetor: ";
  cin >> n;

  int s[n];

  for(int i = 0; i < n; i++) {
    cin >> s[i];
  }
  
  bubbleSort(s, n);

  for(int i = 0; i < n ; i++) 
    cout << s[i] << " ";

  cout << endl;
}

void bubbleSort(int vetor[], int n) { 
 for(int i = 1; i < n ; i++) {
   for(int j = 0; j < n -  1; j++) {
     if(vetor[j] > vetor[j + 1]) {
       int aux = vetor[j];
       vetor[j] = vetor[j + 1];
       vetor[j + 1] = aux;
     }
   }
 }
}
