#include "Set.hpp"

/* Set::Set() {
  elements = 0;
  capacity = CAPACITY;

  table = new int[capacity];

  for (int i = 0; i < capacity; i++) {
    table[i] = -1;
  }
} */

Set::Set(int value) {
  elements = 0;
  capacity = value;

  table = new int[capacity];

  for (int i = 0; i < capacity; i++) table[i] = -1;
}

Set* Set::Union(Set* ConjuntoA, Set* ConjuntoB) {
  Set* ConjuntoC = new Set(ConjuntoA->capacity + ConjuntoB->capacity);
  int sizeA = ConjuntoA->capacity;
  int sizeB = ConjuntoB->capacity;
  int aux = 0;
  for (int i = 0; i < sizeA; i++) ConjuntoC->insert(ConjuntoA->table[i]);

  for (int i = 0; i < sizeB; i++) {
    int number = ConjuntoB->table[i];

    for (int j = 0; j < sizeB; j++) {
      if (ConjuntoC->get(number))
        break;
      else {
        ConjuntoC->insert(number);
      }
    }
  }

  return ConjuntoC;
}

Set* Set::Inter(Set* ConjuntoA, Set* ConjuntoB) {
  Set* ConjuntoC = new Set(ConjuntoA->capacity + ConjuntoB->capacity);
  int sizeA = ConjuntoA->capacity;
  int sizeB = ConjuntoB->capacity;

  for (int i = 0; i < sizeA; i++) {
    for (int j = 0; j < sizeB; j++) {
      if (ConjuntoA->table[i] == ConjuntoB->table[j])
        ConjuntoC->insert(ConjuntoA->table[i]);
    }
  }

  return ConjuntoC;
}

void Set::insert(int key) {
  int index = hashValue(key, 0);
  int start = index;
  bool inserted = false;

  do {
    if (table[index] == -1) {
      table[index] = key;
      inserted = true;
    } else {
      index++;
      if (index == capacity) index = 0;
    }
  } while ((index != start) && (!inserted));

  /* if (!inserted) cerr << "Tabela cheia!" << endl; */
}

bool Set::get(int key) {
  bool found = false;
  bool endSearch = false;
  int index = hashValue(key, 0);
  int startInd = index;

  do {
    if (table[index] == key) {
      endSearch = true;
      found = true;
    } else if (table[index] == -1)
      endSearch = true;
    else {
      index++;
      if (index == capacity) index = 0;
    }
  } while ((!endSearch) && (index != startInd));

  return found;
}

int Set::getCapacity() { return capacity; }

Set::~Set() {
  delete[] table;
  table = NULL;
}

int Set::hashValue(int key, int j) {
  return ((key + (j * STEPSIZE)) % capacity);
}

void Set::print() {
  for (int i = 0; i < capacity; i++) {
    if (table[i] != -1) cout << table[i] << " ";
  }
  cout << endl;
}
