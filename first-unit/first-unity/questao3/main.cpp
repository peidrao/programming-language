/**
 * A partir de elementos de duas pilhas que já estão ordenadas (de maneira decrescente), uma vez que o menor elemento está no topo em ambas. 
 * Acontece a adição dos elementos da duas pilhas em um vetor, para fazer a ordenação dos elementos usando o bubble sort. 
 * Primeiro será adicionado todos os valores da primeira pilha e logo em seguida os elementos da pilha 2.
 * Depois disso, com o vetor já ordenado, acontecerá a adição dos elementos que estão no vetor na fila, e será retornado uma fila ordenada.
 */

#include "Fila.hpp"
#include <iostream>

Fila *ordenrar(Pilha &p1, Pilha &p2);
void bubleSort(Fila &fila, int);
void troca(int *, int *);

int main()
{
    Pilha pilha1(6);
    Pilha pilha2(5);

    pilha1.empilhar(100);
    pilha1.empilhar(70);
    pilha1.empilhar(20);
    pilha1.empilhar(20);
    pilha1.empilhar(10);
    pilha1.empilhar(5);

    pilha2.empilhar(500);
    pilha2.empilhar(70);
    pilha2.empilhar(70);
    pilha2.empilhar(30);
    pilha2.empilhar(10);

    Fila *fila;
    fila = new Fila(pilha1.tamanhoPilha() + pilha2.tamanhoPilha());

    fila = ordenrar(pilha1, pilha2);

    fila->mostrarFila();

    /*  pilha1.~Pilha();
    pilha2.~Pilha();
    fila->~Fila(); */
}

void bubleSort(int vetor[], int n)
{
    int i, j;
    for (i = 0; i < n - 1; i++)

        for (j = 0; j < n - i - 1; j++)
            if (vetor[j] > vetor[j + 1])
                troca(&vetor[j], &vetor[j + 1]);
}

void troca(int *elemento1, int *elemento2)
{
    int temp = *elemento1;
    *elemento1 = *elemento2;
    *elemento2 = temp;
}

Fila *ordenrar(Pilha &p1, Pilha &p2)
{
    Fila *fila;
    fila = new Fila(p1.tamanhoPilha() + p2.tamanhoPilha());
    int tamanho = fila->tamanhoFila();
    int vetor[tamanho];
    int i;

    int tamanho1 = p1.tamanhoPilha();

    for (i = 0; i < tamanho1; i++)
        vetor[i] = p1.desempilhar();

    for (; i < tamanho; i++)
        vetor[i] = p2.desempilhar();

    bubleSort(vetor, tamanho);

    for (int i = 0; i < tamanho; i++)
        fila->adicionar(vetor[i]);

    return fila;
}