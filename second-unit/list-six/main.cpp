#include "Queue.hpp"

int main()
{
    Queue queueList;
    queueList.print();
    queueList.add(2);
    queueList.add(0);
    queueList.add(1);
    queueList.add(8);
    queueList.add(1);
    queueList.add(2);
    queueList.add(0);
    queueList.add(8);
    queueList.add(4);
    queueList.add(4);
    queueList.print();
    cout << "Tamanho da fila: " << queueList.size() << endl;
    cout << (queueList.remove() == 1 ? "ELemento removido!" : "Elemento pode ser removido") << endl;
    queueList.remove();
    queueList.print();
    cout << "Tamanho da fila: " << queueList.size() << endl;

    cout << "Primeiro elemento: " << queueList.get() << endl;
}