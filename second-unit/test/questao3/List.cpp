
#include "List.hpp"

List::List()
{
    head = nullptr;
}

List::~List()
{
    while (head != nullptr)
    {
        Node *current = head;
        head = head->next;
        delete current;
    }
}

void List::add(int value)
{
    Node *aux = new Node(value);
    aux->next = head;
    aux->previous = nullptr;

    if (head != nullptr)
        head->previous = aux;

    head = aux;
}

void List::reverse()
{
    Node *left = head;
    Node *right = head;

    while (right->next != nullptr)
        right = right->next;

    while (left != right && left->previous != right)
    {
        swap(left->value, right->value);
        left = left->next;
        right = right->previous;
    }
}

int List::get(int x)
{
    if (head == nullptr)
        return -1;
    else
    {
        int count = 0;
        Node *current = head;
        while (current != nullptr)
        {
            if (x == current->value)
                return count;
            count += 1;
            current = current->next;
        }
        return -1;
    }
}

void List::print()
{
    Node *h = head;
    if (head == NULL)
        cout << "Lista vázia!" << endl;
    else
    {
        while (h != NULL)
        {
            if (h->next == NULL)
            {
                cout << h->value << " ";
            }
            else
                cout << h->value << "-->";
            h = h->next;
        }
    }
    cout << endl;
}