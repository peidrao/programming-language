#include "Queue.hpp"

Queue::Queue()
{
    head = NULL;
    tail = NULL;
}

Queue::~Queue()
{
    if (head != NULL)
    {
        tail = head;
        head = head->next;
        delete tail;
    }
}

bool Queue::add(int x)
{
    if (head == NULL)
    {
        head = new Node(x);
        tail = head;
    }
    else
    {
        tail->next = new Node(x);
        tail = tail->next;
    }
    return true;
}

int Queue::remove()
{
    Node *aux = head;
    if (head == NULL)
    {
        cout << "Nennum elememno para ser deletado!" << endl;
        return 0;
    }
    else
    {
        aux = head;
        head = head->next;
        delete aux;
        aux = NULL;
        return 1;
    }
}

int Queue::get()
{
    if (head != NULL)
        return head->value;
    return 0;
}

int Queue::size()
{
    Node *aux = head;
    int size = 0;
    while (aux != NULL)
    {
        aux = aux->next;
        size++;
    }
    return size;
}

void Queue::print()
{
    if (head == NULL)
    {
        cout << "Fila sem elementois" << endl;
        return;
    }
    Node *aux = head;
    while (aux->next != NULL)
    {
        cout << aux->value << " ";
        aux = aux->next;
    }
    cout << aux->value;
    cout
        << endl;
}