#include "List.hpp"
#include <iostream>
using namespace std;

int main()
{
    List list;
    list.print();
    list.addElement(1);
    list.addElement(2);
    list.addElement(3);
    list.addLast(4);
    list.addLast(5);

    list.print();                // Listar todos os cinco elementos
    list.addElementIndex(50, 0); // Adiciona o 50 na primeira posição da lista.
    list.print();
    cout << "Tamanho da lista: " << list.listSize() << endl; // O número de elementos que estão na fila, deve imprimir 6.

    list.addElementIndex(100, 0);
    list.addElementIndex(200, 0);
    list.addElementIndex(300, 1);
    list.addElementIndex(500, -1); // Não será adicionado
    list.addElementIndex(500, 50); // Não será adicionado
    list.print();
    cout << "Tamanho da lista: " << list.listSize() << endl;

    list.removeElementIndex(2); // Remove elemento na posição 2, (100)
    list.print();
    cout << "Tamanho da lista: " << list.listSize() << endl;

    list.removeFirstElement(); // Remove primeiro elemento (200).
    list.print();
    cout << "Tamanho da lista: " << list.listSize() << endl;

    list.removeLastElement();
    list.print(); // Remove último elemento (5)
    cout << "Tamanho da lista: " << list.listSize() << endl;

    cout << "Qual elemento deseja procurar: ";
    int elemento;
    cin >> elemento;

    cout << "Elemento: " << elemento << " | Index: " << list.findIndex(elemento) << endl;
    //list.~List();
}