
#ifndef PILHA_HPP
#define PILHA_HPP

class Pilha
{
    char *elementos;
    int capacidade;
    int n;

public:
    Pilha(int);
    ~Pilha();
    void adicionar(int x);
    int remover();
    int tamanhoPilha();
};

#endif
