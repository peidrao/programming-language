#include <iostream>

using namespace std;

int main() {
  int n, x;

  cout << "Defina o tamanho de sua sequência: ";
  cin >> n;

  int s[n];
  
  for(int i = 0; i < n-1 ; i++) {
    cin >> s[i];  
  }


  for(int i = 0; i < n; i++) {
    if(s[i] != i){
      x = i;
      break;
    }
  }

  cout << x << endl;
}

