#ifndef PALINDROMO_HPP
#define PALINDROMO_HPP

class Palindromo
{
private:
    char *palavra;
    int letra;
    int tamanho;

public:
    Palindromo(char palavra[]);
    ~Palindromo();
    void adicionar(char caracter);
    char retirar();
    int verificar(char palavra[]);
};

#endif