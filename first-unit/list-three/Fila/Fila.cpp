#include "Fila.hpp"
#include <iostream>
using namespace std;

Fila::Fila(int capacidade) {
    this->capacidade = capacidade;
    contador = capacidade;
    primeiro = 0;
    ultimo = -1;
    vetor = new int[capacidade];
}

Fila::~Fila(){
    delete[] vetor;
}

void Fila::enfileirar(int elemento) {
    if (filaCheia() != true)
        vetor[++ultimo] = elemento;
    cout << vetor[ultimo] << endl;
}

int Fila::desenfileirar() {
    return vetor[primeiro++];
}

bool Fila::filaCheia() {
    if (tamanhoFila() >= contador)
        return true;
    return false;
}

bool Fila::filaVazia() {
    if (primeiro > ultimo)
        return true;
    return false;
}

int Fila::tamanhoFila() {
    return (ultimo - primeiro) + 1;
}

int Fila::elementoFundo() {
    return vetor[ultimo];
}

int Fila::elementoTopo() {
    return vetor[primeiro];
}

void Fila::mostrarFila() {
    for (int i = primeiro; i < ultimo + 1; i++)
        cout << vetor[i] << " ";
    cout << endl;
}