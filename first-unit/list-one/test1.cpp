#include <iostream>

using namespace std;

int buscaSequencial(int vetor[], int tam, int chave);

int main() {
  int n, x;
  
  cout << "Digite um inteiro: ";
  cin >> n;

  int s[n];

  for(int i = 0; i < n ; i++) {
    cout << "Número " << i+1 << ": " ;
    cin >> s[i];
  }

  cin >> x;

  cout <<  buscaSequencial(s, n, x) << endl;
}

int buscaSequencial(int vetor[], int tam, int chave) {
  for(int i = 0; i < tam; i++) {
    if(vetor[i] == chave) return i;
  }
  return -1;
}

