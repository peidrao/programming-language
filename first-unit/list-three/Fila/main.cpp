#include "Fila.hpp"

#include <iostream>
using namespace std;

int main() {
    Fila fila(5);

    //Adição de quatro elementos
    fila.enfileirar(1);
    fila.enfileirar(2);
    fila.enfileirar(3);
    fila.enfileirar(4);

    fila.mostrarFila(); // Mostrar elementos que estão na fila
    cout << "Tamanho da fila: " << fila.tamanhoFila() << endl;
    fila.desenfileirar();
    cout << (fila.filaCheia() == true ? "Fila Cheia!\n" : "Fila vazia!\n");
    fila.mostrarFila();
    cout << "Tamanho da fila: " << fila.tamanhoFila() << endl;

    fila.enfileirar(5);
    fila.enfileirar(6);
    fila.enfileirar(7); // Elemento não vai ser inserido pois a fila está cheia
    fila.mostrarFila();
    cout << "Tamanho da fila: " << fila.tamanhoFila() << endl;
    cout << (fila.filaCheia() == true ? "Fila Cheia!\n" : "Fila vazia!\n");

    fila.~Fila();
}