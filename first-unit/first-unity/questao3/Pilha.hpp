#ifndef PILHA_HPP
#define PILHA_HPP

#include <iostream>
using namespace std;

class Pilha
{
private:
    int *elementos;
    int capacidade;
    int topo;

public:
    Pilha(int);
    ~Pilha();
    void empilhar(int);
    int desempilhar();
    int pilhaTopo();
    int tamanhoPilha();
    int pilhaVazia();
    int elementoIndex(int);
};

#endif