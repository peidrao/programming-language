#include <iostream>

using namespace std;

int removerDuplicados(int arr[], int n);
int main() {
  int n;

  cout << "Digite a quantidade de elementos de sua sequência: ";
  cin >> n;

  int s[n];
  int s2[n];

  for (int i = 0; i< n; i++) s2[i] =0;

  cout << "Digite os " << n << " elementos de modo ordenado." << endl;
  for(int i = 0; i < n ; i++) {
    cin >> s[i];
    s2[i] = 0;
  }

  
  int cont = 0;
  for(int i = 0; i < n ; i++) {
    for(int j = i+1; j < n; j++) {
      if(s[i] == s[j]) {  
        s2[cont] = s[i];
        cont++;
      }
              
    }
  }

 if(cont > 0) {
int  aux = removerDuplicados(s2, cont);

  for(int i = 0; i < aux ; i++) {
  cout << s2[i] << " ";
  }


  }
  
  cout << endl;

}

int removerDuplicados(int arr[], int n) {   
    int temp[n]; 
  
    // Start traversing elements 
    int j = 0; 
    for (int i=0; i<n-1; i++) 
  
        // If current element is not equal 
        // to next element then store that 
        // current element 
        if (arr[i] != arr[i+1]) 
            temp[j++] = arr[i]; 
  
    // Store the last element as whether 
    // it is unique or repeated, it hasn't 
    // stored previously 
    temp[j++] = arr[n-1]; 
  
    // Modify original array 
    for (int i=0; i<j; i++) 
        arr[i] = temp[i]; 
  
    return j; 
} 

