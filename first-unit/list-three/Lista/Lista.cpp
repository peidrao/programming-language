#include "Lista.hpp"
#include <iostream>

using namespace std;

Lista::Lista(int capacidade) {
    this->capacidade = capacidade;
    tamanho = 0;
    elementos = new int[capacidade];
}

Lista::~Lista() {
    delete[] elementos;
}

int Lista::adicionar(int valor, int posicao) {
    if (tamanho < capacidade) {
        if (posicao == tamanho) 
            elementos[posicao] = valor;
        else {
            for (int i = tamanho; i > posicao; i--)
                elementos[i] = elementos[i - 1];
            elementos[posicao] = valor;
        }
        tamanho += 1;
        cout << "Adicionando elemento: " << valor << endl;
        return 1;
    }
    else
        return 0;
}

int Lista::tamanhoLista() {
    return tamanho;
}

void Lista::remover(int pos) {
    for (int i = pos; i < tamanho - 1; i++)
        elementos[i] = elementos[i + 1];
    tamanho -= 1;
}

int Lista::elemento(int pos) {
    return elementos[pos];
}

int Lista::buscar(int valor) {
    int index = -1;
    for (int i = 0; i < tamanho; i++) {
        if (valor == elementos[i]) {
            index = i;
            break;
        }
    }
    return index;
}


void Lista::mostrarLista() {
    cout << "Elementos da fila: "; 
    for (int i = 0; i < tamanhoLista(); i++) 
        cout << elementos[i] << " ";
}