    int i = 0,
        pilha1 = 0, pilha2 = 0, resultado = 0;

    string str = "";

    while (i < valor.length())
    {
        while (isspace(valor[i]))
            i++;

        if (isdigit(valor[i]) | valor[i] == '.')
        {
            while (isdigit(valor[i]) | valor[i] == '.')
            {
                str += valor[i];
                i++;
            }
            adicionar((atof(str.c_str()))); // Atof = converter string -> int;
            str = "";
        }
        else if (valor[i] == '+' || valor[i] == '-' || valor[i] == '*' || valor[i]  == '/')
        {
            if (valor[i] == '+')
            {
                pilha1 = retirar();
                pilha2 = retirar();
                resultado = (pilha1 + pilha2);
            }
            if (valor[i] == '-')
            {
                pilha1 = retirar();
                pilha2 = retirar();
                resultado = pilha2 - pilha1;
            }
            if (valor[i] == '*')
            {
                pilha1 = retirar();
                pilha2 = retirar();
                resultado = (pilha1 * pilha2);
            }
            if (valor[i] == '/')
            {
                pilha1 = retirar();
                pilha2 = retirar();
                resultado = (pilha2 / pilha1);
            }
            
            i++;
            
            adicionar(resultado);
        }
        else {
            cout << "inválido!" << endl;
        }
    }

    return retirar();