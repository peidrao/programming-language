#include <iostream>

using namespace std;

int main() {
  int n;

  cout << "Digite a quantidade de elementos de sua sequência: ";
  cin >> n;

  int s[n];

  int maiorNumero = 0;
  int menorNumero = 100000;
  
  for(int i = 0; i < n ; i++) {
    cin >> s[i];
  }

  for(int i = 0; i < n ; i++) {
    if(s[i] > maiorNumero) maiorNumero = s[i];
    if(s[i] < menorNumero) menorNumero = s[i];
  }

  cout << menorNumero << " " << maiorNumero << endl;
}
