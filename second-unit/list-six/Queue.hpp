
#ifndef Queue_H_
#define Queue_H_

#include "Node.hpp"
#include <iostream>
using namespace std;

class Queue
{
	Node *head;
	Node *tail; // ultimo elemento

public:
	Queue();
	~Queue();
	bool add(int x); // adiciona um elemento no final da fila
	int remove();	 // remove o primeiro elemento inserido na fila
	int get();		 // recupera, mas não remove, o primeiro elemento inserido na fila
	int size();		 // retorna a quantidade de elementos na fila
	void print(); 	// Listar todos os elementos da fila
};

#endif