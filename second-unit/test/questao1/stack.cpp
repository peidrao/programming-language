#include "stack.hpp"
#include <iostream>
using namespace std;

Stack::Stack()
{
    head = NULL;
    temp.cont = -1;
}

Stack::~Stack()
{
    Node *current;
    while (head != NULL)
    {
        current = head;
        head = head->next;
        delete current;
    }
}

bool Stack::isEmpty()
{
    return (temp.cont == -1) ? true : false;
}

void Stack::add(int value)
{
    Node *aux = new Node(value);
    aux->next = NULL;

    if (isEmpty())
    {
        head = aux;
        head->next = NULL;
    }
    else
    {
        aux->next = head;
        head = aux;
    }
    temp.cont++;
}

int Stack::remove()
{
    int value;
    if (isEmpty())
        cout << "Nenhum elemento na pilha!" << endl;
    Node *aux;
    if (head != NULL)
    {
        aux = head;
        value = head->value;
        head = head->next;

        delete aux;
        temp.cont--;
    }
    return value;
}

int Stack::get()
{
    if (isEmpty())
    {
        cout << "Lista Vázia!" << endl;
        return -1;
    }
    else
    {
        cout << "Elemento do topo: " << head->value << endl;
        return head->value;
    }
}

int Stack::size()
{
    if (head == NULL)
        return 0;

    Node *h = head;
    int size = 0;

    while (h)
    {
        h = h->next;
        size++;
    }
    cout << "Tamanho: " << size << endl;
    return size;
}

void Stack::print()
{
    Node *h = head;
    if (head == NULL)
        cout << "Lista vázia!" << endl;
    else
    {
        while (h != NULL)
        {
            if (h->next == NULL)
            {
                cout << h->value << " ";
            }
            else
                cout << h->value << "-->";
            h = h->next;
        }
    }
    cout << endl;
}
