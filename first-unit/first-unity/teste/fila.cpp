#include "fila.hpp"
#include <iostream>

using namespace std;

Fila::Fila(int capacidade)
{
    this->capacidade = capacidade;
    contador = capacidade;
    primeiro = 0;
    ultimo = -1;
    vetor = new int[capacidade];

    cout << "O vetor tem: " << capacidade << " elementos!" << endl;
}

Fila::~Fila()
{
    delete[] vetor;
}

void Fila::adicionar(int elemento)
{
    if (cheia() != true)
    {
        ++ultimo;
        vetor[ultimo] = elemento;
        cout << vetor[ultimo] << endl;
    }
}

bool Fila::cheia()
{
    if (tamanho() >= contador)
        return true;
    return false;
}

bool Fila::vazia()
{
    if (primeiro > ultimo)
        return true;
    return false;
}

int Fila::tamanho()
{
    return (ultimo - primeiro + 1);
}

int Fila::retirar()
{
    return vetor[primeiro++];
}

int Fila::fundo()
{
    return vetor[ultimo];
}

int Fila::topo()
{
    return vetor[primeiro];
}