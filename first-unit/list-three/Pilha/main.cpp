#include "Pilha.hpp"
#include <iostream>

using namespace std;

int main() {
    Pilha pilha(3);

    pilha.empilhar(1);
    pilha.empilhar(2);
    cout << "elemento do topo: " << pilha.elementoTopo() << endl;
    pilha.empilhar(3);
    pilha.mostrarPilha();
    pilha.desempilhar();
    
    pilha.empilhar(4);


    pilha.mostrarPilha();

    pilha.~Pilha();
}