#ifndef NODE_HPP
#define NODE_HPP

class Node
{
public:
    int value;
    int cont;
    Node *next;
    Node(int);
    Node();
};

#endif
