#ifndef PILHA_HPP
#define PILHA_HPP

#include "Fila.hpp"

class Pilha
{
private:
    Fila *fila1, *fila2;

public:
    Pilha(int);
    ~Pilha();
    void adicionarPilha(int);
    int retirarPilha();
    int tamanhoPilha();
    int Elemento();
    void ElementoTopo();
};

#endif