
#ifndef LIST_H_
#define LIST_H_

#include <iostream>
using namespace std;
#include "Node.hpp"

class List
{
	Node *head;

public:
	List();
	~List();

	void addFront(int);
	void addLast(int);

	int deleteFront();
	int deleteLast();

	int getFront();
	int getLast();

	void print();

	int remove();
	int size();
};

#endif