#ifndef STACK_HPP
#define STACK_HPP

#include "Node.hpp"
#include <iostream>
using namespace std;

class Stack
{
private:
    Node *head;
    Node temp;

public:
    Stack();
    ~Stack();
    bool isEmpty();
    void add(int);
    int remove();
    int get();
    void print();
    int size();
};

#endif
