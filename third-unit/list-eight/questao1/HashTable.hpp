#ifndef HASHE_TABLE_HPP
#define HASHE_TABLE_HPP

#include <iostream>
using namespace std;

const int CAPACITY = 11;
const int STEPSIZE = 3;

class HashTable
{
private:
    int *table;
    int elements;
    int capacity;

public:
    HashTable();
    HashTable(int);
    ~HashTable();
    void insert(int);
    int hashValue(int key, int j);

    bool get(int);
    int getCapacity();
};

#endif