#include <iostream>

using namespace std;

void merge(int*, int, int, int);
void mergeSort(int*, int, int);

int main() {
  int n;
  
  cout << "Quantos elementos deseja adicionar: ";
  cin >> n;

  int s[n];

  for(int i = 0; i < n ; i++) 
    cin >> s[i];
    
  mergeSort(s, 0, n-1);

  for(int i = 0; i < n ; i++) {
    cout << s[i] << " ";
  }

  cout << endl;
}


void merge(int vetor[], int primeiroIndex, int ultimoIndex, int n) {
  int i, j, k,
      subVetor1 = n - primeiroIndex + 1,
      subVetor2 = ultimoIndex - n,
      vetorPrimeiro[subVetor1],
      vetorSegundo[subVetor2];


  for(i = 0 ; i < subVetor1 ; i++) 
      vetorPrimeiro[i] = vetor[primeiroIndex + i];

   for(j = 0 ; j < subVetor2 ; j++) 
      vetorSegundo[j] = vetor[n + 1  + j];

    
  i = 0;
  j = 0;
  k = primeiroIndex;

  while(i < subVetor1 && j < subVetor2) {
    if(vetorPrimeiro[i] <= vetorSegundo[j]) {
      vetor[k] = vetorPrimeiro[i];
      i++;
    } else {
      vetor[k] = vetorSegundo[j];
      j++;
    }
    k++;
  }
  while(i < subVetor1) {
    vetor[k] = vetorPrimeiro[i];
    i++;
    k++;
  }
  while(j < subVetor2) {
    vetor[k] =vetorSegundo[j];
    j++;
    k++;
  }
}


void mergeSort(int vetor[], int primeiroIndex, int ultimoIndex) {
  if(primeiroIndex < ultimoIndex) {
    int meio = primeiroIndex + (ultimoIndex - primeiroIndex) / 2;

    mergeSort(vetor, primeiroIndex, meio);
    mergeSort(vetor, meio + 1, ultimoIndex);

    merge(vetor, primeiroIndex, ultimoIndex, meio);
  }
}
