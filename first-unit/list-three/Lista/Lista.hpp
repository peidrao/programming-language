#ifndef LISTA_HPP
#define LISTA_HPP

class Lista
{

    int capacidade;
    int tamanho;
    int *elementos;

public:
    Lista(int capacidade);
    ~Lista();
    int adicionar(int valor, int posicao);
    int tamanhoLista();
    int buscar(int valor);
    void remover(int pos);
    int elemento(int pos);
    void mostrarLista();
};

#endif