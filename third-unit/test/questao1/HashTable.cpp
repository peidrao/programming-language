#include "HashTable.hpp"

HashTable::HashTable() {
  elements = 0;
  capacity = CAPACITY;

  table = new int[capacity];

  for (int i = 0; i < capacity; i++) {
    table[i] = -1;
  }
}

HashTable::HashTable(int value) {
  elements = 0;
  capacity = value;

  table = new int[capacity];

  for (int i = 0; i < capacity; i++) table[i] = -1;
}

void HashTable::addRest(int key) {
  int index = hashValue(key, 0);
  int start = index;
  bool inserted = false;

  do {
    if (table[index] == -1) {
      table[index] = key;
      inserted = true;
    } else {
      index++;
      if (index == capacity) index = 0;
    }
  } while ((index != start) && (!inserted));

  if (!inserted) cerr << "Tabela cheia!" << endl;
}

bool HashTable::get(int key) {
  bool found = false;
  bool endSearch = false;
  int index = hashValue(key, 0);
  int startInd = index;

  do {
    if (table[index] == key) {
      endSearch = true;
      found = true;
    } else if (table[index] == -1)
      endSearch = true;
    else {
      index++;
      if (index == capacity) index = 0;
    }
  } while ((!endSearch) && (index != startInd));

  return found;
}

int HashTable::getCapacity() { return capacity; }

HashTable::~HashTable() {
  delete[] table;
  table = NULL;
}

int HashTable::hashValue(int key, int j) {
  return ((key + (j * STEPSIZE)) % capacity);
}

void HashTable::print() {
  for (int i = 0; i < capacity; i++) {
    if (table[i] != -1) cout << table[i] << " ";
  }
  cout << endl;
}

int HashTable::hashMulti(int key, int size) {
  //  const int w = 32;
  const long long w_bit = 4294967295;
  const int A = 2654435769;
  long long r0 = key * A;
  return ((r0 & w_bit) >> (32 - size));
}

void HashTable::addMulti(int key) {
  int index = hashMulti(key, 0);
  int start = index;
  bool inserted = false;

  do {
    if (table[index] == -1) {
      table[index] = key;
      inserted = true;
    } else {
      index++;
      if (index == capacity) index = 0;
    }
  } while ((index != start) && (!inserted));

  if (!inserted) cerr << "Tabela cheia!" << endl;
}