#include <cstdlib>
#include <ctime>
#include <iostream>

#include "HashTable.hpp"

using namespace std;

int main() {
  HashTable rest(10);
  HashTable multi(10);
  // int seed = 20844;
  srand(time(NULL));

  int vetor[100];
  for (int i = 0; i < 100; i++) {
    vetor[i] = (rand() % 1000);
  }

  for (int i = 0; i < 10; i++) {
    rest.addRest(vetor[i]);
  }

  for (int i = 0; i < 10; i++) {
    multi.addMulti(vetor[i]);
  }

  multi.print();
  rest.print();
}