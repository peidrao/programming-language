
#ifndef LIST_H_
#define LIST_H_

#include "Node.hpp"

#include <iostream>
using namespace std;
class List
{
    Node *head;

public:
    List();
    ~List();

    void add(int);

    void reverse();
    void print();

    int get(int x);
};

#endif