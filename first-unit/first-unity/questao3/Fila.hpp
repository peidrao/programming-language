#ifndef FILA_HPP
#define FILA_HPP

#include "Pilha.hpp"

class Fila
{
private:
    int *elementos;
    int primeiro, ultimo;
    int tamanho;

public:
    Fila(int);
    ~Fila();
    void adicionar(int valor);
    int remover();
    void mostrarFila();
    int filaVazia();
    int filaCheia();
    int filaTopo();
    int tamanhoFila();
};

#endif