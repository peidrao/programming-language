/* 
Para a resolução dessa questão foi usado uma função de inversão (reverse), para fazer a inversão de toda a lista. Para que pudesse acontecer a troca
dos elementos de posição, foi usado uma função da própria biblioteca do c++ (http://www.cplusplus.com/reference/algorithm/swap/).

A lógica do método reverse, foi ter dois nós um apontando para o início (left) e outro para o final (right) e fazermos as operações de troca de cada elemento.
*/

#include "List.hpp"

int main()
{
    List list, list2, list3;

    list.add(10);
    list.add(20);
    list.add(30);
    list.add(40);
    list.add(50);
    list.add(60);
    list.add(70);
    list.add(80);
    list.add(90);
    list.add(100);

    list.reverse();

    cout
        << "Elemento 10 | Posição: " << list.get(10) << endl;
    cout << "Elemento 20 | Posição: " << list.get(20) << endl;
    cout << "Elemento 30 | Posição: " << list.get(30) << endl;
    cout << "Elemento 40 | Posição: " << list.get(40) << endl;
    cout << "Elemento 50 | Posição: " << list.get(50) << endl;
    cout << "Elemento 60 | Posição: " << list.get(60) << endl;
    cout << "Elemento 70 | Posição: " << list.get(70) << endl;
    cout << "Elemento 80 | Posição: " << list.get(80) << endl;
    cout << "Elemento 90 | Posição: " << list.get(90) << endl;
    cout << "Elemento 100 | Posição: " << list.get(100) << endl;

    list.print();
    list.reverse();

    list.add(110);
    list.add(120);
    list.add(130);
    list.add(140);
    list.add(150);
    list.reverse();

    list.print();
    list.add(160);
    list.add(170);
    list.add(180);
    list.add(190);
    list.add(200);
    list.reverse();

    list.print();
    list.~List();

    cout << "\n\nComeçando exemplo 2\n\n";

    list2.add(1);
    list2.add(2);
    list2.add(3);
    list2.add(4);
    list2.add(5);
    cout << "Elemento 1 | Posição: " << list2.get(1) << endl;
    cout << "Elemento 2 | Posição: " << list2.get(2) << endl;
    cout << "Elemento 3 | Posição: " << list2.get(3) << endl;
    cout << "Elemento 4 | Posição: " << list2.get(4) << endl;
    cout << "Elemento 5 | Posição: " << list2.get(5) << endl;
    list2.print();
    list2.reverse();
    list2.print();
    cout << "Elemento 1 | Posição: " << list2.get(1) << endl;
    cout << "Elemento 2 | Posição: " << list2.get(2) << endl;
    cout << "Elemento 3 | Posição: " << list2.get(3) << endl;
    cout << "Elemento 4 | Posição: " << list2.get(4) << endl;
    cout << "Elemento 5 | Posição: " << list2.get(5) << endl;
    list2.add(6);
    list2.add(7);
    list2.add(8);
    list2.add(9);
    list2.add(10);
    list2.print();
    list2.reverse();
    list2.print();

    list2.add(20);
    list2.add(30);
    list2.add(40);
    list2.add(50);
    list2.add(60);
    list2.add(70);
    list2.add(80);
    list2.add(90);
    list2.add(100);
    list2.add(110);
    list2.print();
    list2.reverse();
    list2.print();

    cout << "Elemento 10 | Posição: " << list2.get(20) << endl;
    cout << "Elemento 20 | Posição: " << list2.get(30) << endl;
    cout << "Elemento 30 | Posição: " << list2.get(40) << endl;
    cout << "Elemento 40 | Posição: " << list2.get(50) << endl;
    cout << "Elemento 50 | Posição: " << list2.get(60) << endl;
    cout << "Elemento 60 | Posição: " << list2.get(70) << endl;
    cout << "Elemento 70 | Posição: " << list2.get(80) << endl;
    cout << "Elemento 80 | Posição: " << list2.get(90) << endl;
    cout << "Elemento 90 | Posição: " << list2.get(100) << endl;
    cout << "Elemento 100 | Posição: " << list2.get(110) << endl;
    list2.print();
    list2.reverse();
    list2.print();
    list2.~List();

    cout << "\n\nComeçando exemplo 3\n\n";

    list3.add(1);
    list3.add(2);
    list3.add(3);
    list3.add(4);
    list3.add(5);
    list3.add(6);
    list3.add(7);
    list3.add(8);
    list3.add(9);
    list3.add(10);
    list3.print();
    list3.reverse();
    list3.print();
    list3.reverse();
    list3.reverse();
    list3.reverse();

    cout << "Elemento 1 | Posição: " << list3.get(1) << endl;
    cout << "Elemento 2 | Posição: " << list3.get(2) << endl;
    cout << "Elemento 3 | Posição: " << list3.get(3) << endl;
    cout << "Elemento 4 | Posição: " << list3.get(4) << endl;
    cout << "Elemento 5 | Posição: " << list3.get(5) << endl;
    cout << "Elemento 6 | Posição: " << list3.get(6) << endl;
    cout << "Elemento 7 | Posição: " << list3.get(7) << endl;
    cout << "Elemento 8 | Posição: " << list3.get(8) << endl;
    cout << "Elemento 9 | Posição: " << list3.get(9) << endl;
    cout << "Elemento 10 | Posição: " << list3.get(10) << endl;

    list3.reverse();
    list3.reverse();
    list3.reverse();
    list3.reverse();
    list3.reverse();
    list3.reverse();
    list3.reverse();
    list3.reverse();
    list3.print();

    list3.add(11);
    list3.add(12);
    list3.add(13);
    list3.add(14);
    list3.add(15);
    list3.print();

    cout << "Elemento 11 | Posição: " << list3.get(11) << endl;
    cout << "Elemento 12 | Posição: " << list3.get(12) << endl;
    cout << "Elemento 13 | Posição: " << list3.get(13) << endl;
    cout << "Elemento 14 | Posição: " << list3.get(14) << endl;
    cout << "Elemento 15 | Posição: " << list3.get(15) << endl;
    list3.reverse();
    list3.reverse();
    list3.reverse();
    list3.print();
    cout << "Elemento 11 | Posição: " << list3.get(11) << endl;
    cout << "Elemento 12 | Posição: " << list3.get(12) << endl;
    cout << "Elemento 13 | Posição: " << list3.get(13) << endl;
    cout << "Elemento 14 | Posição: " << list3.get(14) << endl;
    cout << "Elemento 15 | Posição: " << list3.get(15) << endl;

    list3.add(16);
    list3.add(17);
    list3.add(18);
    list3.add(19);
    list3.add(20);
    list3.print();
    list3.reverse();
    list3.reverse();
    list3.print();
}