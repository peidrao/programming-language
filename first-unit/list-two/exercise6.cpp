#include <iostream>
using namespace std;

void selectionSort(int *, int, int, int);

int main()
{
  int n, x, y; // x é o primeiro valor e z é o segundo valor

  cout << "Digite o tamnho da posição: ";
  cin >> n;

  cout << "posições x: ";
  cin >> x;
  cout << "posição y: ";
  cin >> y;

  int s[n];

  for (int i = 0; i < n; i++)
  {
    cin >> s[i];
  }

  selectionSort(s, n, x, y);

  cout << endl;

  return 0;
}

void selectionSort(int vetor[], int n, int x, int y)
{
  int aux, meioIndex;

  for (int i = x; i < y - 1; i++)
  {
    meioIndex = i;

    for (int j = i + 1; j < y; j++)
    {
      if (vetor[j] < vetor[meioIndex])
      {
        meioIndex = j;
      }

      aux = vetor[meioIndex];
      vetor[meioIndex] = vetor[i];
      vetor[i] = aux;
    }
  }

  for (int i = 0; i < n; i++)
    cout << vetor[i] << " ";
}
