
#ifndef FILA_HPP
#define FILA_HPP

class Fila
{
    int *elementos;
    int capacidade;
    int n;
    int topo;
    int fim;

public:
    Fila(int capacity);
    ~Fila();
    void enfileirar(int x);
    int remover();
    int elementoTopo();
    int tamanho();
};

#endif
