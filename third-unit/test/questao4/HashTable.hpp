#ifndef HASHTABLE_HPP
#define HASHTABLE_HPP

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class Hashtable {
 private:
  static const int tableSize = 4;
  struct item {
    int value;
    string world;
    item* next;
  };
  item* Hashing[tableSize];

 public:
  Hashtable();
  int hash(int);
  void Add(int, string);

  string FindWorld(int);
  int FindValue(string);
};

#endif