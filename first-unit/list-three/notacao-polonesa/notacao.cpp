#include "notacao.hpp"
#include <iostream>
using namespace std;

Notacao::Notacao(int tamanho) {
    aux = 0;
    this->tamanho = tamanho;
    elementos = new int[tamanho];
}

Notacao::~Notacao() {
    delete[] elementos;
}

bool Notacao::PilhaCheia() {
    return aux > tamanho - 1;
}


void Notacao::adicionar(int valor) {
    if (PilhaCheia())
        return;
    aux++;
    elementos[aux] = valor;
}

int Notacao::retirar() {
    int _aux = elementos[aux];
    aux--;
    return _aux;
}

int Notacao::notacao(string valor)
{

}