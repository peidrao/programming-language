#include "Fila.hpp"

Fila::Fila(int tamanho)
{
    this->tamanho = tamanho;
    elementos = new int[tamanho];
    primeiro = -1;
    ultimo = -1;
}

Fila::~Fila()
{
    delete[] elementos;
}

void Fila::adicionar(int valor)
{
    // elementos[++ultimo] = valor;
    if (filaVazia())
    {
        primeiro++;
        ultimo++;
        elementos[ultimo] = valor;
    }
    else if (filaCheia())
        cout << "FIla cheia" << endl;
    else
    {
        ultimo = (ultimo + 1) % tamanho;
        elementos[ultimo] = valor;
    }
}

int Fila::remover()
{
    if (filaVazia())
        return 1;
    else if (primeiro == ultimo)
    {
        int a = elementos[primeiro];
        primeiro = ultimo = -1;
        return a;
    }
    else
    {
        int a = elementos[primeiro];
        primeiro = (primeiro + 1) % tamanho;
        return a;
    }
}

void Fila::mostrarFila()
{
    for (int i = 0; i < tamanho; i++)
        cout << elementos[i] << " ";
    cout << endl;
}

int Fila::tamanhoFila()
{
    return tamanho;
}

int Fila::filaVazia()
{
    return (primeiro == -1) ? 1 : 0;
}

int Fila::filaCheia()
{
    return ((ultimo + 1) % tamanho == primeiro) ? 1 : 0;
}

int Fila::filaTopo()
{
    return elementos[primeiro];
}
