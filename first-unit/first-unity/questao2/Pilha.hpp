#ifndef PILHA_HPP
#define PILHA_HPP

class Pilha
{
private:
    int *elementos;
    int capacidade;
    int topo;

public:
    Pilha(int);
    ~Pilha();
    void empilhar(int);
    int desempilhar();
    int elementoTopo();
    bool pilhaCheia();
    int tamanhoPilha();
    int elementoIndex(int);
};

#endif