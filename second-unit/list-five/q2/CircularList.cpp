#include "CircularList.hpp"

CircularList::CircularList()
{
    head = NULL;
}

CircularList::~CircularList()
{
    if (head != NULL)
    {
        Node *current = head->next;
        while (current != head)
        {
            Node *aux = current;
            current = current->next;
            delete aux;
        }
    }
}

int CircularList::listSize()
{
    Node *node = head;
    int cont = 0;

    if (head == NULL)
    {
        cout << "Lista vázia!" << endl;
        return 0;
    }
    else
    {
        cont++;
        while (node->next != head)
        {
            node = node->next;
            cont++;
        }
        return cont;
    }
}

void CircularList::printList()
{
    Node *node = head;

    if (head == NULL)
    {
        cout << "Lista vázia!" << endl;
        return;
    }

    while (node->next != head)
    {
        cout << node->value << "-->";
        node = node->next;
    }
    cout << node->value << "\n\n";
}

void CircularList::addNode(int value)
{
    Node *newNode = new Node(value);

    if (head == NULL)
    {
        newNode->next = newNode;
        head = newNode;
        cout << "Adicionando no primeiro nó: " << value << endl;
        return;
    }

    Node *aux = head;
    while (aux->next != head)
        aux = aux->next;

    if (head != NULL)
    {
        newNode->next = (head);
        aux->next = (newNode);
        head = newNode;
        cout << "Adicionando elemento: " << value << endl;
    }
}

int CircularList::findNode(int value)
{
    Node *aux = head;
    while (aux->next != head)
    {
        if (aux->value == value)
            return value;
        aux = aux->next;
    }

    if (value == aux->value)
        return value;

    return -1;
}