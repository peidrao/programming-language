/**
 * A ideia dessa implementação é usar a estrutura já feita de pilha e apenas implementar uma 
 * estrutura pilha com duas operações que são a adição e remoção.
 * Para o método de adição, será feito uma verificação na fila1, caso ela esteja 
 * vazia será adicionada o valor nela, caso contrário isso representará que a fila1 já possui elementos 
 * Para que possamos adicionar na fila1, se faz necessário adicionar todos os elementos que estavam na 
 * fila1 para a fila2, logo após isso, o valor desejado será adicionado na fila1 e os elementos que estavam
 * na fila2, passam novamente para fila1.
 * 
 * O método de remover, faz apenas a remoção do último elemento da nossa pilha.
 */

#include "Pilha.hpp"
#include <iostream>

using namespace std;

int main()
{
    Pilha pilha(5);

    pilha.adicionarPilha(2);
    pilha.adicionarPilha(3);
    pilha.adicionarPilha(4);
    pilha.adicionarPilha(5);
    pilha.adicionarPilha(6);
    pilha.retirarPilha(); // Retira o elemento 6

    pilha.ElementoTopo(); // Imprime o elemento 5

    pilha.retirarPilha(); // Remove o elemento 5

    pilha.ElementoTopo(); // Imprime o elemento 4

    pilha.~Pilha();
}