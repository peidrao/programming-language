#include "Fila.hpp"
#include <iostream>

using namespace std;

Fila::Fila(int capacidade)
{
    this->capacidade = capacidade;
    pilha1 = new Pilha(capacidade);
    pilha2 = new Pilha(capacidade);
}

Fila::~Fila()
{
    delete pilha1;
    delete pilha2;
}

void Fila::enfileirar(int elemento)
{
    while (pilha1->tamanhoPilha() > 0)
    {
        pilha2->empilhar(pilha1->desempilhar());
    }
    pilha1->empilhar(elemento);
    while (pilha2->tamanhoPilha() > 0)
    {
        pilha1->empilhar(pilha2->desempilhar());
    }
}

int Fila::desenfileirar()
{
    if (pilha1->tamanhoPilha() == 0)
        cout << "Fila está vazia!" << endl;
    else
    {
        int x = pilha1->desempilhar();
        pilha1->desempilhar();
        cout << "Desempilhando: " << x << endl;
        return x;
    }
    return 0;
}

int Fila::primeiro()
{
    return pilha1->elementoTopo();
}

void Fila::primentoElemento()
{
    if (pilha1->elementoTopo())
        cout << "Primeiro elemento da fila: " << pilha1->elementoTopo() << endl;
}