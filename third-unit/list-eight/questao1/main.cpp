#include "HashTable.hpp"

int main()
{
    int vetor[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
    int elements = 11;

    HashTable hash;

    for (int i = 0; i < (elements / 2); ++i)
        hash.insert(vetor[i]);

    cout << endl;

    for (int i = elements - 1; i >= 0; --i)
    {
        if (hash.get(vetor[i]))
            cout << vetor[i] << " Encontrado." << endl;
        else
            cout << vetor[i] << " Não encontrado." << endl;
    }

    cout << (hash.get(100) ? "Encontrado." : "Não Encontrado.") << endl;

    hash.insert(35);
    cout << endl;

    return 0;
}